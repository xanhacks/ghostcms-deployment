---
# Install packages
- name: Install docker & usefull packages
  become: yes
  ansible.builtin.apt:
    pkg:
      - vim
      - wget
      - curl
      - docker.io
      - docker-compose
    state: present
    update_cache: yes

# Services
- name: Enable and start docker
  become: yes
  ansible.builtin.systemd:
    name: docker
    state: started
    enabled: yes

# User config
- name: Add user to docker group
  become: yes
  ansible.builtin.shell: "usermod -aG docker {{ ansible_user_id }}"

# Create configuration directories
- name: Create configuration directories
  become: yes
  ansible.builtin.file:
    path: "{{ path_to_installation }}/{{ item }}"
    state: directory
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
  loop:
    - nginx
    - nginx/conf.d
    - nginx/letsencrypt
    - nginx/letsencrypt/live
    - nginx/letsencrypt/live/{{ nginx_ghost_server_name.split(' ')[0] }}
    - nginx/letsencrypt/live/{{ nginx_matomo_server_name }}
    - nginx/letsencrypt/live/{{ nginx_grafana_server_name }}

- name: Create configuration directories
  become: yes
  ansible.builtin.file:
    path: "{{ path_to_installation }}/{{ item }}"
    state: directory
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
  loop:
    - grafana
    - prometheus
  when: enable_observability

# Send configuration files
- name: Copy static configuration files
  become: yes
  ansible.builtin.copy:
    src: "./files/{{ item }}"
    dest: "{{ path_to_installation }}/{{ item }}"
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
    mode: '0644'
  loop:
    - nginx/nginx.conf
    - nginx/letsencrypt/live/{{ nginx_ghost_server_name.split(' ')[0] }}/fullchain.pem
    - nginx/letsencrypt/live/{{ nginx_ghost_server_name.split(' ')[0] }}/privkey.pem

- name: Copy static configuration files for observability
  become: yes
  ansible.builtin.copy:
    src: "./files/{{ item }}"
    dest: "{{ path_to_installation }}/{{ item }}"
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
    mode: '0644'
  loop:
    - grafana/datasource.yml
    - prometheus/prometheus.yml
    - nginx/letsencrypt/live/{{ nginx_grafana_server_name }}/fullchain.pem
    - nginx/letsencrypt/live/{{ nginx_grafana_server_name }}/privkey.pem
  when: enable_observability

- name: Copy static configuration files for analytics
  become: yes
  ansible.builtin.copy:
    src: "./files/{{ item }}"
    dest: "{{ path_to_installation }}/{{ item }}"
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
    mode: '0644'
  loop:
    - nginx/letsencrypt/live/{{ nginx_matomo_server_name }}/fullchain.pem
    - nginx/letsencrypt/live/{{ nginx_matomo_server_name }}/privkey.pem
  when: enable_analytics

- name: Copy .env
  become: yes
  ansible.builtin.copy:
    src: files/.env
    dest: "{{ path_to_installation }}/.env"
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
    mode: '0400'

- name: Copy template configuration files
  become: yes
  ansible.builtin.template:
    src: "./{{ item }}.j2"
    dest: "{{ path_to_installation }}/{{ item }}"
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
    mode: '0644'
  loop:
    - docker-compose.yml
    - nginx/conf.d/00-ghost.conf

- name: Copy template configuration file for analytics
  become: yes
  ansible.builtin.template:
    src: "./nginx/conf.d/01-matomo.conf.j2"
    dest: "{{ path_to_installation }}/nginx/conf.d/01-matomo.conf"
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
    mode: '0644'
  when: enable_analytics

- name: Copy template configuration file for observability
  become: yes
  ansible.builtin.template:
    src: "./{{ item }}.j2"
    dest: "{{ path_to_installation }}/{{ item }}"
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_id }}"
    mode: '0644'
  loop:
    - nginx/conf.d/02-grafana.conf
  when: enable_observability
