# Ghost

[GhostCMS](https://ghost.org/) website deployment with [Ansible](https://www.ansible.com/).

## Architecture

| Name           | Optional | Description          |
| :------------- |:--------:| :------------------- | 
| Ghost          | False    | CMS for the website. |
| Nginx          | False    | Reverse proxy for the website (Ghost), analytics dashboard (Matomo) and the observability dashboard (Grafana). |
| MySQL          | False    | A relational database for Ghost. |
| Matomo         | True     | Self-hosted analytics tool. |
| MySQL          | True     | A relational database for for Matomo. |
| Prometheus     | True     | Time series database to save real-time metrics. |
| cAdvisor       | True     | Fetch observability metrics from Docker containers. |
| Grafana        | True     | Monitoring dashboards on a web application. |

## Setup

1. **Configuration**

- Configure your Ansible inventory, ex: [sandb](inventories/sandb) or [prod](inventories/prod).
- Configure the default deployment variables in [main.yml](roles/deploy/defaults/main.yml).
- Rename [.env.sample](roles/deploy/files/.env.sample) to .env, then edit it.
- Create folders at [roles/deploy/files/nginx/letsencrypt/live/](roles/deploy/files/nginx/letsencrypt/live/) named with the server names (ghost, matomo and grafana) and deposit fullchain.pem and privkey.pem in it. (Ex: Create three folders named *example.com*, *stats.example.com* and *graphs.example.com* at [roles/deploy/files/nginx/letsencrypt/live/](roles/deploy/files/nginx/letsencrypt/live/) and deposit fullchain.pem and privkey.pem to each of them.) You can generate self-signed certificate by using this openssl command `openssl req -x509 -newkey rsa:4096 -keyout privkey.pem -out fullchain.pem -sha256 -days 365 -nodes`.

2. **Run the Ansible playbook**

```shell
$ ansible-playbook -i inventories/sandb deploy.yml
```

> Easiest way to install Ansible :
>
> ```$ python -m pip install --user ansible```
>
> Official installation docs, [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
